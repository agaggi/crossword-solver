# Crossword Puzzle Solver

This program solves crossword puzzles through the use of backtracking. When the
program cannot make any more moves, then the last word placed is removed and
another option is tried.

## Crossword Puzzles

There are two puzzles this program solves. In general, it can solve puzzles in
a nxm layout. Here are the crossword puzzles provided:

- `*`'s represent the **beginning** of a wordspace
- `_`'s represents areas where letters can go

### Easy

```
*_*_*
##_#_
#*_*_
*#*__
*____
_##_#
```

### Hard

```
##*_____*####
##_#####_##*#
##_#####_##_#
##_##*_*__#_#
##_####_###_#
####*##*_*__#
####_##_#_#_#
#*#*____#_###
#_##_##_#_###
*_____###_###
#_##_####_###
```

## Program Requirements

- Python 3.7 or newer

## Execution

The program should be executed as follows:

```
# Unix-based
./main.py {difficulty}

# Windows
py .\main.py {difficulty}
```

where *difficulty* is either **easy** or **hard**.

## Results

In general, each difficulty runs as follows:

### Easy

```
HOSES
##A#T
#HIKE
A#LEE
LASER
E##L#


Time taken to solve: 0.0004398822784423828
Number of backtracks: 12
```

### Hard

```
##abalone####
##b#####a##a#
##a#####c##b#
##c##aloha#a#
##k####c###c#
####a##coypu#
####b##u#a#s#
#a#babel#m###
#b##t##t#m###
obtain###e###
#e##s####r###


Time taken to solve: 0.01992511749267578
Number of backtracks: 14
```
