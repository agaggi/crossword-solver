import copy
import time

from crossword_entry import CrosswordEntry
import utility as util

class Solver:

    def __init__(self, difficulty: str) -> None:

        '''Class implements methods that read crossword and wordlist input files and solves
        a provided crossword puzzle through backtracking.

        Args:
            difficulty: The difficulty of the crossword puzzle (easy or hard)
        '''

        self.difficulty = difficulty

        self.num_backtracks = 0
        self.backtrack = False

        self.start_time = None
        self.entries = []


    def process_input_files(self) -> None:

        '''Processes the crossword and wordlist input files and creates a list of
        wordspaces with words that can fit in them.'''

        with open(f'crossword_puzzles/{self.difficulty}.txt', 'r') as crossword_file:

            crossword = crossword_file.read().split()

        with open(f'wordlists/{self.difficulty}.txt', 'r') as wordlist_file:

            wordlist = wordlist_file.read().split()

        for i, row in enumerate(crossword):

            for j, char in enumerate(row):

                # Check whether a space can be across, down, or both
                if char == '*':

                    if util.is_across(crossword, i, j):

                        word_length = util.get_across_length(crossword, i, j)
                        valid_words = [word for word in wordlist if len(word) == word_length]

                        self.entries.append(CrosswordEntry('Across', (i, j), valid_words))

                    if util.is_down(crossword, i, j):

                        word_length = util.get_down_length(crossword, i, j)
                        valid_words = [word for word in wordlist if len(word) == word_length]

                        self.entries.append(CrosswordEntry('Down', (i, j), valid_words))

        self.start_time = time.time()
        self.make_move(crossword, 0, [])


    def make_move(self, crossword: list[str], index: int, used_words: list[str]) -> None:

        '''Places a word in a wordspace. Takes into account letters already on the path.

        This function uses recursion to simulate the backtracking of moves. If we cannot
        place another word on the crossword, then we "backtrack", meaning we go back a word
        and try a different option.

        Args:
            crossword: The current crossword puzzle
            index: The current index we are on in the `self.entries` variable
            used_words: The words that have been used so far
        '''

        util.print_crossword(crossword)
        crossword_copy = copy.deepcopy(crossword)

        if util.check_completion(crossword):

            exit(f'''
Time taken to solve: {time.time() - self.start_time}
Number of backtracks: {self.num_backtracks}
''')

        for word in self.entries[index].possible_words:

            if self.backtrack:

                print('--> Backtracking...')

                # Remove the word that caused the backtrack and the one used before it as well
                used_words.pop()
                crossword = util.remove_last_word(crossword, self.entries[index-1])

                self.num_backtracks += 1
                self.backtrack = False

            if word not in used_words:

                row, col = self.entries[index].start_point
                curr_letter = 0

                # Flag used to determine whether a word is actually viable
                viable_word = True

                if self.entries[index].direction == 'Across':

                    # ... (start of word) to (start of word + word length) ...
                    for j in range(col, col + len(word)):

                        # If where we are at on the crossword is a letter, we must make
                        # sure the current letter of our possible word matches
                        if crossword_copy[row][j].isalpha():

                            if not word[curr_letter] == crossword_copy[row][j]:

                                viable_word = False
                                break

                        curr_letter += 1

                    # If no errors, then insert the word and move on to the next entry
                    if viable_word:

                        new_row = [crossword_copy[row][:col], word, crossword_copy[row][col+len(word):]]
                        crossword_copy[row] = ''.join(new_row)

                        used_words.append(word)
                        self.make_move(crossword_copy, index+1, used_words)

                if self.entries[index].direction == 'Down':

                    # ...
                    # (Start of word)
                    # to
                    # (End of word)
                    # ...
                    for i in range(row, row + len(word)):

                        if crossword_copy[i][col].isalpha():

                            if not word[curr_letter] == crossword_copy[i][col]:

                                viable_word = False
                                break

                        # Insert the current letter of the word in the current row
                        if viable_word:

                            new_col = crossword_copy[i][:col], word[curr_letter], crossword_copy[i][col+1:]
                            crossword_copy[i] = ''.join(new_col)

                        curr_letter += 1

                    # If no errors, then move on to the next entry
                    if viable_word:

                        used_words.append(word)
                        self.make_move(crossword_copy, index+1, used_words)

        # If we reach this point, we exhausted all our current options and need to backtrack
        self.backtrack = True
