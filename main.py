#! /usr/bin/env python3

import sys

from solver import Solver

def main() -> None:

    try:

        difficulty = sys.argv[1].lower()

        if difficulty == 'easy' or difficulty == 'hard':

            solver = Solver(difficulty)
            solver.process_input_files()

    except IndexError:

        print('Not enough arguments entered, see README file for valid arguments')


if __name__ == '__main__':

    main()
