import dataclasses

@dataclasses.dataclass
class CrosswordEntry:

    direction: str
    start_point: tuple[int]
    possible_words: list[str]
